# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/core/x86_64/curl/
## Maintainer : Dave Reisner <dreisner@archlinux.org>
## Contributor: Angel Velasquez <angvp@archlinux.org>
## Contributor: Eric Belanger <eric@archlinux.org>
## Contributor: Lucien Immink <l.immink@student.fnt.hvu.nl>
## Contributor: Daniel J Griffiths <ghost1227@archlinux.us>
#--------------------------------------------------------------------------------------

pkgname=curl
pkgver=7.64.1
pkgrel=3
arch=('x86_64')
license=('MIT')
_website="https://curl.haxx.se"
pkgdesc="An URL retrieval utility and library"
url="https://curl.haxx.se/download"
source=("$url/$pkgname-$pkgver.tar.gz"{,.asc})

provides=(
    'libcurl.so')
options=(
    'strip'
    'debug')

depends=(
    'ca-certificates'
    'krb5'
    'libssh2'
    'libressl'
    'zlib'
    'libpsl'
    'libnghttp2')

#--------------------------------------------------------------------------------------
build() {
    cd "$pkgname-$pkgver"

    ./configure                                             \
        --prefix=/usr                                       \
        --mandir=/usr/share/man                             \
        --enable-ipv6                                       \
        --enable-threaded-resolver                          \
        --enable-versioned-symbols                          \
        --disable-ldap                                      \
        --disable-ldaps                                     \
        --disable-manual                                    \
        --with-gssapi                                       \
        --with-libssh2                                      \
        --with-random=/dev/urandom                          \
        --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt
    make
}

package() {
    cd "$pkgname-$pkgver"

    make DESTDIR="$pkgdir" install
    make DESTDIR="$pkgdir" install -C scripts

    ## license
    install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

#--------------------------------------------------------------------------------------
validpgpkeys=('27EDEAF22F3ABCEB50DB9A125CC908FDB71E12C2' # Daniel Stenberg
)
sha512sums=('c8f8c4397e0e2975e7553f36637b7e7caa29d7953229dcf4d8051f9bae0cf55572d6e25fc27d9c34fe8783cf87893d96d447601c74662374b8bbd393b6cb6825'
            'SKIP')
